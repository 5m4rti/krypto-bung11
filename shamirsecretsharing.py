import random

def IsPrime(n, s = 50): # Miller-Rabin-Primzahltest
  # Aufruf: IsPrime(n,s) mit natuerlichen Zahlen n,s
  # Ausgabe: True oder False
  #     n prim => Ausgabe True mit Wkt. 1
  #     n nicht prim => Ausgabe True mit Wkt. <= 1/(2**s)
  if n < 2: return False
  for j in range(1, s + 1):
    a = random.randint(1, n - 1)
    i = n - 1
    b = []
    while (i > 0):
      b.append(i % 2)
      i = i // 2
    d = 1
    for i in range(len(b) - 1, -1, -1):
      x = d
      d = (d * d) % n
      if d == 1 and x != 1 and x != n - 1:
        return False
      if b[i] == 1:
        d = (d * a) % n
    if d != 1:
      return False
  return True

def is_prime(n, s = 50):
  return IsPrime(n,s)

def egcd(ld,d): # erweiterter Euklidischer Algorithmus
  # Aufruf: egcd(a,b) mit natuerlichen Zahlen a,b>0
  # Ausgabe: (d,x,y) mit:
  #     d ist groesster gemeinsamer Teiler von a und b
  #     x,y sind ganze Zahlen mit d = x*a + y*b
  (lx,x) = (1,0)
  (ly,y) = (0,1)
  while d != 0:
    q = ld//d
    (d,ld) = (ld%d,d)
    (x,lx) = (lx-q*x,x)
    (y,ly) = (ly-q*y,y)       
  return (ld,lx,ly)

def gcd(a,b): # groesster gemeinsamer Teiler
  # Aufruf: gcd(a,b) mit natuerlichen Zahlen a,b>0
  # Ausgabe: groesster gemeinsamer Teiler von a und b
  return egcd(a,b)[0]

def ModInv(e,n): # Inverses mod n
  # Aufruf: ModInv(e,n) mit natuerlichen Zahlen e,n>0 und ggT(e,n)=1
  # Ausgabe: d aus {1,...,n-1} mit (d*e)%n = 1
  (d,x,y)=egcd(e,n)
  return x%n

########################################################
########################################################
########################################################
def get_prime(low, high):
  number = random.randint(low, high)
  while(not is_prime(number)):
    number = random.randint(low, high)
  return number

def SSSEncode(n,t,s):   # Shamir-Secret-Sharing: Geheimnis zerlegen
  # Aufruf: SSSEncode(n,t,s) mit natuerlichen Zahlen n,t,s mit 2<=t<=n
  # Ausgabe: (p,t,y) mit
  #     p = Primzahl > max(n+1,s+1)
  #     t = Wert aus der Eingabe
  #     y = Hashmap der Geheimnisteile y[1],...,y[n]
  p = get_prime(max(n+1,s+1),10*max(n+1,s+1))
  a_list = []
  for i in range(1,t):
    a_list.append(random.randint(1,p)) # 1 bis p oder 1 bis p-1
  y = {}
  for x in range(1,n+1):
    part = s
    power = 1
    for a in a_list:
      part += (a*x**power) % p
      part = part % p
      power += 1
    y[x]=part
  return (p,t,y)

def SSSDecode(p,t,y):   # Shamir-Secret-Sharing: Geheimnis rekonstruieren
  # Aufruf: SSSDecode(p,t,y) mit den von SSSEncode ausgegebenen Zahlen p,t und
  #     y = Hashmap, die mindestens t Werte der von SSSEncode
  #         ausgegebenen Hashmap enthaelt
  # Ausgabe: s = rekonstruiertes Geheimnis
  #
  s = 0
  for key in y.keys()[0:t]:
    prod = y[key]
    for j in set(y.keys()[0:t])-{key}:
      prod *= j * ModInv(j-key%p, p)
    s += prod % p
  return s%p

def SSSTest():  # zum Testen des Shamir-Secret-Sharing
  n=20
  t=17
  s=1234567890
  (p,t,y)=SSSEncode(n,t,s)
  #
  for i in range(0,10):
    s = SSSDecode(p,t,y)
    print("von " + str(20-i) + " Teilnehmern rekonstruiertes Geheimnis: "+str(s))
    del y[i+1]
  return
