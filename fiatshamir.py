import random

def IsPrime(n, s = 50): # Miller-Rabin-Primzahltest
  # Aufruf: IsPrime(n,s) mit natuerlichen Zahlen n,s
  # Ausgabe: True oder False
  #     n prim => Ausgabe True mit Wkt. 1
  #     n nicht prim => Ausgabe True mit Wkt. <= 1/(2**s)
  if n < 2: return False
  for j in range(1, s + 1):
    a = random.randint(1, n - 1)
    i = n - 1
    b = []
    while (i > 0):
      b.append(i % 2)
      i = i // 2
    d = 1
    for i in range(len(b) - 1, -1, -1):
      x = d
      d = (d * d) % n
      if d == 1 and x != 1 and x != n - 1:
        return False
      if b[i] == 1:
        d = (d * a) % n
    if d != 1:
      return False
  return True

def egcd(ld,d): # erweiterter Euklidischer Algorithmus
  # Aufruf: egcd(a,b) mit natuerlichen Zahlen a,b>0
  # Ausgabe: (d,x,y) mit:
  #     d ist groesster gemeinsamer Teiler von a und b
  #     x,y sind ganze Zahlen mit d = x*a + y*b
  (lx,x) = (1,0)
  (ly,y) = (0,1)
  while d != 0:
    q = ld//d
    (d,ld) = (ld%d,d)
    (x,lx) = (lx-q*x,x)
    (y,ly) = (ly-q*y,y)       
  return (ld,lx,ly)

def gcd(a,b): # groesster gemeinsamer Teiler
  # Aufruf: gcd(a,b) mit natuerlichen Zahlen a,b>0
  # Ausgabe: groesster gemeinsamer Teiler von a und b
  return egcd(a,b)[0]

########################################################
########################################################
########################################################
def is_prime(number, s = 50):
  return IsPrime(number, s)

def get_prime(low, high):
  number = random.randint(low, high)
  while(not is_prime(number)):
    number = random.randint(low, high)
  return number

def get_inversible(n):
  inversible = random.randint(1, n-1)
  while(gcd(inversible,n) != 1):
    inversivle = random.randint(1, n-1)
  return inversible

def fs_key_gen(l=512): # Fiat-Shamir-Identifikation: Geheimnis + Public-Key erzeugen
  # Aufruf: FSKeyGen(l) mit natuerlicher Zahl l>2
  # Ausgabe: (pk,sk) = ((v,n),(s,n)) mit
  #     n = p*q fuer zufaellige l-Bit Primzahlen p,q
  #     s zufaellig mit 1<=s<=n-1 und ggT(s,n)=1
  #     v = (s*s mod n)
  p = get_prime(2**(l-1), 2**l-1)
  q = get_prime(2**(l-1), 2**l-1)
  n = p * q
  s = get_inversible(n)
  v = pow(s,2,n)
  pk = (v,n)
  sk = (s,n)
  return (pk,sk)

def fs_one_round(pk,sk): # Fiat-Shamir-Identifikation: Durchfuehrung einer Runde
  # Aufruf: FSOneRound(pk,sk) mit public key pk und secure key sk
  # Ausgabe: True,  falls Identifikation erfolgreich
  #          False, falls Identifikation nicht erfolgreich
  (v,n) = pk
  s = sk[0]

  p_r = get_inversible(n)
  p_x = pow(p_r,2,n)
  v_a = random.randint(0,1)
  p_y = p_r * pow(s,v_a,n) % n
  return 1<=p_x<n and gcd(p_x,n)==1 and pow(p_y,2,n)==p_x*pow(v,v_a,n)%n

def fs_test(l=100): # zum Testen der Fiat-Shamir-Identifikation
  (pk,sk)=fs_key_gen(l)
  for i in range(0,20):
    if fs_one_round(pk,sk)==True:
      print("Runde erfolgreich")
    else:
      print("Runde nicht erfolgreich")
  return

fs_test()
