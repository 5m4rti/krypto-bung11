import random
import diffie_hellman as dh

def ElgamalKeyGen(l=256):
  # Aufruf: ElgamalKeyGen(l) mit natuerlicher Zahl l>=4
  # Ausgabe: ((p,g,B),(p,g,b)) mit
  #     p = zufaellige l-Bit Primzahl, sodass (p-1)/2 prim ist
  #     g = zufaelliger Erzeuger mod p, d.h. {g**i | 0<i<p} = {1,...,p-1}
  #     b = zufaelliges Element aus {0,...,p-2}
  #     B = (g**b)%p
  (g,p) = dh.DHChooseGenerator(l)
  b = random.randint(1,p-1)
  B = pow(g,b,p) # beta = g**b mod p
  return ((p,g,B),(p,g,b))

def mes_to_mod_p_list(m, p):
  result = list()
  done = False
  cur_m = m
  while(not done):
    mod = cur_m % p
    div = cur_m / p
    result.append(mod)
    cur_m = div
    if(cur_m == 0):
      done = True
  return result

def ElgamalEncrypt(pk,m):
  # Aufruf: ElgamalEncrypt(pk,m) mit public key pk und Klartext m
  # Ausgabe: Chiffretext c
  (p,g,B) = pk 
  a = random.randint(1,p-1)
  A = pow(g,a,p)
  chiffre = ((pow(B,a,p)*m)%p)
  return (A,chiffre)

def ElgamalDecrypt(sk,c):
  # Aufruf: ElgamalDecrypt(sk,c) mit secure key sk und Chiffretext c
  # Ausgabe: dechiffrierte Nachricht
  (p,g,b) = sk
  (A,chiffre) = c
  x = p-1-b
  m = (pow(A,x,p)*chiffre)%p  
  return m

def int2str(x): # codiert Zahl als String
  # Aufruf: int2str(23268733837745479405720608239248647353390)
  # Ausgabe: 'Das ist ein Test.'
  s = ''
  while(x>0):
    s = chr(x&255) + s
    x = x >> 8
  return s

def str2int(s): # codiert String als Zahl
  # Aufruf: str2int('Das ist ein Test.')
  # Ausgabe: 23268733837745479405720608239248647353390
  x = 0
  for i in range(0,len(s)):
    x = (x<<8) + ord(s[i])
  return x

def ElgamalTest(ms = 'Geheimnis'):
  m=str2int(ms)
  print("Klartext als String:           "+ms)
  print("Klartext als Zahl:             "+str(m))
  (pk,sk)=ElgamalKeyGen(90)
  print("Public Key:                    "+str(pk))
  print("Private Key:                   "+str(sk))
  c1=ElgamalEncrypt(pk,m)
  c2=ElgamalEncrypt(pk,m)
  print("1. Chiffretext:                "+str(c1))
  print("2. Chiffretext:                "+str(c2))
  b1=ElgamalDecrypt(sk,c1)
  b2=ElgamalDecrypt(sk,c2)
  print("1. Chiffretext entschluesselt: "+str(b1))
  print("2. Chiffretext entschluesselt: "+str(b2))
  b1s=int2str(b1) 
  b2s=int2str(b2) 
  print("1. Chiffretext entschluesselt: "+b1s)
  print("2. Chiffretext entschluesselt: "+b2s)
  return
