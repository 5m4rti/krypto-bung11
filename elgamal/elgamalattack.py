import random
import hashlib
import elgamal

def IsPrime(n, s = 50): # Miller-Rabin-Primzahltest
  # Aufruf: IsPrime(n,s) mit natuerlichen Zahlen n,s
  # Ausgabe: True oder False
  #     n prim => Ausgabe True mit Wkt. 1
  #     n nicht prim => Ausgabe True mit Wkt. <= 1/(2**s)
  # Laufzeit bei Eingabe (n,s): O(s * |n|**2)
  if n < 2: return False
  for j in range(1, s + 1):
    a = random.randint(1, n - 1)
    i = n - 1
    b = []
    while (i > 0):
      b.append(i % 2)
      i = i // 2
    d = 1
    for i in range(len(b) - 1, -1, -1):
      x = d
      d = (d * d) % n
      if d == 1 and x != 1 and x != n - 1:
        return False
      if b[i] == 1:
        d = (d * a) % n
    if d != 1:
      return False
  return True

def egcd(ld,d): # erweiterter Euklidischer Algorithmus
  # Aufruf: egcd(a,b) mit natuerlichen Zahlen a,b>0
  # Ausgabe: (d,x,y) mit:
  #     d ist groesster gemeinsamer Teiler von a und b
  #     x,y sind ganze Zahlen mit d = x*a + y*b
  (lx,x) = (1,0)
  (ly,y) = (0,1)
  while d != 0:
    q = ld//d
    (d,ld) = (ld%d,d)
    (x,lx) = (lx-q*x,x)
    (y,ly) = (ly-q*y,y)       
  return (ld,lx,ly)

def gcd(a,b): # groesster gemeinsamer Teiler
  # Aufruf: gcd(a,b) mit natuerlichen Zahlen a,b>0
  # Ausgabe: groesster gemeinsamer Teiler von a und b
  return egcd(a,b)[0]

def ModInv(e,n): # Inverses mod n
  # Aufruf: ModInv(e,n) mit natuerlichen Zahlen e,n>0 und ggT(e,n)=1
  # Ausgabe: d aus {1,...,n-1} mit (d*e)%n = 1
  (r,x,d)=egcd(n,e)
  while(d<0):
    d+=n
  return d


########################################################
########################################################
########################################################

def Hash(m):    # liefert fuer String m den SHA256-Hashwert aus {0,...,2**256-1}
  h=hashlib.sha256(bytearray(m,'UTF8')) # in Bytearray umwandeln fuer SHA
  s=h.hexdigest()                       # String mit Hashwert in Hexadezimaldarstellung
  return int(s,16)                      # Hashwert als Zahl aus {0,...,2**256-1}

def ElgamalSignature(sk,m):     # signiert eine Nachricht m
  # Aufruf: ElgamalSignature(sk,m) mit secure key sk und Nachricht m (String)
  # Ausgabe: signierte Nachricht sig = (m,r,s)
  (p,g,a) = sk
  k=0
  r=0
  k2=0
  s=0
  while(s==0):
    # k mit ggt(k,p-1)=1
    k = random.randint(1,p-2)
    while(gcd(k,p-1) != 1):
      k = random.randint(1,p-2)
    # r = g^k mod p
    r = pow(g,k,p)
    # k' = k^-1
    k2 = ModInv(k,p-1)
    # s = k' * (h(x) - ar) mod (p-1)
    s = k2 * (Hash(m)-(a*r)) % (p-1)
  return (m,r,s)

def ElgamalSignatureVerify(pk,sig): # ueberprueft Signatur einer Nachricht
  # Aufruf: ElgamalSignatureVerify(pk,sig) mit pk=public key, sig=signierte Nachricht
  # Ausgabe: True, falls Signatur korrekt, False sonst
  (p,g,A) = pk
  (m,r,s) = sig
  conditions = []
  conditions.append(1<=r<=p-1)
  conditions.append(0<s<p-1)
  left_side = (pow(A,r,p)*pow(r,s,p)) % p
  right_side = pow(g,Hash(m),p)
  conditions.append(left_side == right_side)
  # test all conditions
  return False not in conditions

def ElgamalSignatureTest():     # Beispiel zur Elgamal-Signatur
  (pk,sk)=elgamal.ElgamalKeyGen(128)
  m = str(7777**7777)           # Klartext mit 30259 Zeichen
  sig=ElgamalSignature(sk,m)
  print("signiertes Dokument: sig = "+str(sig))
  print("Verifikation: "+str(ElgamalSignatureVerify(pk,sig)))
  return

def broken_sig(sk, m, k):
  # returns a signed message (m,r,s), that is signed using the given k
  (p,g,a) = sk
  r = pow(g,k,p)
  # k' = k^-1
  k2 = ModInv(k,p-1)
  # s = k' * (h(x) - ar) mod (p-1)
  s = k2 * (Hash(m)-(a*r)) % (p-1)
  return (m,r,s)

def find_solution(a,b,n, checker):
  # Find solutions to equations in the form of: a*x=b (mod n).
  # returns the first solution with checker(solution)==True
  print("find_solution("+str(a)+","+str(b)+","+str(n)+") was called:")
  print("(" + str(a) + "* x = " + str(b) + " ) mod(" + str(n) + ")")
  (g,A,N) = egcd(a,n)
  q = b // g
  if(g * q != b):
    print "ERROR: gcd_g teilt b nicht"
    return
  else:
    print("g == " + str(g))
    sol0 = (A*b/g)
    sol0 = sol0 % n
    print('sol0: ' + str(sol0))
    for k in range(0,g):
      solk = sol0 + k*(n/g)
      solk = solk % n
      print('checking sol'+str(k)+': ' + str(solk))
      if(checker(solk)):
        print("found k=" + str(solk))
        return (solk) % n
    print "error, no solution found"
    return sol0

def attack(msg1, msg2, pk):
  # Tries to find the private key, that was used to sign the two messages.
  # msg1 and msg2 must be signed using equal values of k
  (m1, r1, s1) = msg1
  (m2, r2, s2) = msg2
  r = r1
  (p,g,A) = pk
  # k * left = right
  left = (s1-s2)
  right = (Hash(m1)-Hash(m2))
  checker = lambda sol: pow(g,sol,p)==r
  k = find_solution(left,right,p-1, checker)#((Hash(m1)-Hash(m2)) * ModInv(s1-s2,p-1))%(p-1)
  print('Found k: ' + str(k))
  print('checker(k): ' + str(checker(k)))
  left = r
  right = (Hash(m2) -s2*k)
  checker = lambda sol: pow(g, sol, p)==A
  a = find_solution(left,right,p-1,checker)
  print('gcd(r,p-1): ' + str(gcd(r, p-1)))
  return a % (p-1)

def test_attack(keylength=128):
  # Signs two messages with the same k value and tests if attack yields the correct private k
  (pk,sk)=elgamal.ElgamalKeyGen(keylength)
  (p,g,A) = pk
  m1 = "important message"
  m2 = "irrelavant message"

  while(True):
    k = random.randint(1,p-2)
    while(gcd(k,p-1) != 1):
      k = random.randint(1,p-2)

    sig1 = broken_sig(sk, m1, k)
    sig2 = broken_sig(sk, m2, k)
    if(sig1[2]!=0 and sig2[2]!=0):
      print('Used k: ' + str(k))
      break

  a = attack(sig1, sig2, pk)
  print sk[2]
  print a
  if(a == sk[2]):
    print "sk was found"
  else:
    print "sk was not found"

test_attack(8)
