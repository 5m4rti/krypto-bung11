import random

def IsPrime(n, s = 50): # Miller-Rabin-Primzahltest
  # Aufruf: IsPrime(n,s) mit natuerlichen Zahlen n,s
  # Ausgabe: True oder False
  #     n prim => Ausgabe True mit Wkt. 1
  #     n nicht prim => Ausgabe True mit Wkt. <= 1/(2**s)
  if n < 2: return False
  for j in range(1, s + 1):
    a = random.randint(1, n - 1)
    i = n - 1
    b = []
    while (i > 0):
      b.append(i % 2)
      i = i // 2
    d = 1
    for i in range(len(b) - 1, -1, -1):
      x = d
      d = (d * d) % n
      if d == 1 and x != 1 and x != n - 1:
        return False
      if b[i] == 1:
        d = (d * a) % n
    if d != 1:
      return False
  return True

def random_number(l):
  return random.randint(2**l,2**(l+1))

def is_generator(g, factors, p):
  for factor in factors:
    cant_be_generator = pow(g,(p-1)/factor,p) == 1 #folgerung 4.25
    if(cant_be_generator):
      return False
  return True

def DHChooseGenerator(l=256):   # Erzeugerwahl fuer Diffie-Hellman
  # Aufruf: DHChooseGenerator(l) mit natuerlicher Zahl l>=4
  # Ausgabe: (g,p) mit
  #     p = zufaellige l-Bit Primzahl, sodass (p-1)/2 prim ist
  #         (d.h. (p-1)/2 ist eine Sophie-Germain-Primzahl)
  #     g = zufaelliger Erzeuger mod p, d.h. {(g**i)%p | 0<i<p} = {1,...,p-1}
  p = random_number(l)
  sophie_germain = (p-1)/2
  while(not IsPrime(p) or not IsPrime(sophie_germain)):#while p-1/2 is not sophie germain prime
    p = random_number(l)
    if(p%2 == 0):
      p-=1
    sophie_germain = (p-1)/2

  factors = (2,(p-1)/2)
  g = random.randint(1,p-1)
  while(not is_generator(g, factors, p)):
    g = random.randint(1,p-1)
  return (g,p)

def DHTest():   # Diffie-Hellman-Test
  (g,p)=DHChooseGenerator()
  # hier soll ein Schluesselaustausch zwischen Alice und Bob simuliert werden
  print "Erzeuger g und Primzahl p wurden bestimmt"
  a = random.randint(1,p-1)
  alpha = pow(g,a,p) # alpha = g**a mod p 
  print "Alice waehlt geheimes a=",a,"und verschickt alpha=",alpha
  b = random.randint(1,p-1)
  beta = pow(g,b,p) # beta = g**b mod p
  print "Bob waehlt geheimes b=",b,"und verschickt beta=",beta
  key_alice = pow(beta,a,p)
  key_bob = pow(alpha,b,p)
  print "Alice berechnet key_alice=",key_alice
  print "Bob berechnet key_bob=",key_bob
  if(key_alice == key_bob):
    print "Der Schluesselaustausch war erfolgreich"
  else:
    print "Der Schluesselaustausch war nicht erfolgreich"
